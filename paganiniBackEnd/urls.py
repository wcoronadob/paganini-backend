from django.conf import settings
from django.urls import include, path
from django.conf.urls.static import static
from django.contrib import admin
from core.views import IndexView, LoginView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='homepage'),
    path('documents/', include('documents.urls')),
    path('batch/', include('batch.urls')),
    path('login/', LoginView.as_view(), name='login'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.MEDIA_ROOT)



from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import FormView
from django.views.generic.base import TemplateView
from .forms import LoginForm


class IndexView(LoginRequiredMixin, TemplateView):
    login_url = '/login/'
    template_name = "index.html"


class LoginView(FormView):
    success_url = '/'
    form_class = LoginForm
    template_name = "login.html"

    def form_valid(self, form):
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        if user is not None:
            login(self.request, user)
            return redirect('/')
        else:
            messages.error(self.request, 'User Not Exists contact with Human Resources plz', extra_tags='alert')
            return redirect('/login/')

        return super(LoginView, self).form_valid(form)

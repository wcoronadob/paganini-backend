import os
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from django.contrib.auth.models import User
from batch.models import Batch


class DocumentType(models.Model):
    type = models.CharField(max_length=255)

    def __str__(self):
        return self.type


def get_latest_batch_to_add_documents():
    batch = Batch.objects.filter(is_uploaded=False).order_by('-id')[0]
    return batch


def get_image_path(instance, filename):
    batch = get_latest_batch_to_add_documents()
    return os.path.join('documents', str(batch.correlative), filename)


class Document(models.Model):
    observation = models.TextField(blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_upload = models.BooleanField(default=False)
    type = models.ForeignKey('documents.DocumentType', on_delete=models.CASCADE)
    image = models.ImageField(
        upload_to=get_image_path
    )
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)

    def get_batch_obj(self):
        batch = Batch.objects.get(documents=self.pk)
        return batch

    class Meta:
        ordering = ['-date_created']


@receiver(post_save, sender=Document)
def document_post_save(sender, **kwargs):
    batch = get_latest_batch_to_add_documents()
    document = kwargs['instance']
    obj = batch.documents.add(document)




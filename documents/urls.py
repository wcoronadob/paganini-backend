from django.urls import path
from .views import \
    DocumentListView, \
    NewDocumentView, \
    DocumentDetailView

urlpatterns = [
    path('', DocumentListView.as_view(), name='document-list'),
    path('new/', NewDocumentView.as_view(), name='document-new'),
    path('detail/<int:pk>', DocumentDetailView.as_view(), name='document-detail')
]

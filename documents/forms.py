from django import forms
from crispy_forms.helper import FormHelper
from .models import DocumentType


class NewDocumentsForm(forms.Form):
    observation = forms.CharField(widget=forms.Textarea(), required=False)
    type = forms.ModelChoiceField(queryset=DocumentType.objects.all())
    image = forms.ImageField()

    def __init__(self, *args, **kwargs):
        super(NewDocumentsForm, self).__init__(*args, **kwargs)
        self.fields['image'].widget.attrs['capture'] = ''



from django.shortcuts import redirect, render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import NewDocumentsForm
from .models import Document


class DocumentListView(LoginRequiredMixin, ListView):
    model = Document
    queryset = Document.objects.filter(is_upload=False)
    login_url = '/login/'
    template_name = 'Documents/list.html'


class DocumentDetailView(LoginRequiredMixin, DetailView):
    model = Document
    login_url = '/login/'
    template_name = 'Documents/detail.html'


class NewDocumentView(LoginRequiredMixin, FormView):
    form_class = NewDocumentsForm
    login_url = '/login/'
    template_name = 'Documents/new.html'
    success_url = '/documents'

    def form_valid(self, form):
        obs = form.cleaned_data['observation']
        type_doc = form.cleaned_data['type']
        img = form.cleaned_data['image']
        user = self.request.user
        obj = Document(observation=obs, type=type_doc, image=img, user=user)
        obj.save()
        return super(NewDocumentView, self).form_valid(form)


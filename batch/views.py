from django.shortcuts import render
from django.core.paginator import Paginator
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import Batch


class BatchListView(ListView):
    model = Batch
    template_name = 'Batch/list.html'


class BatchDetailView(DetailView):
    model = Batch
    context_object_name = 'batch_info'
    template_name = 'Batch/detail.html'

    def get_context_data(self, **kwargs):
        context = super(BatchDetailView, self).get_context_data(**kwargs)
        context['documents'] = self.get_related_documents()
        return context

    def get_related_documents(self):
        queryset = Batch.objects.get(pk=self.object.pk).documents.all()
        paginator = Paginator(queryset, 5)
        page_number = self.request.GET.get('page')
        documents = paginator.get_page(page_number)
        return documents

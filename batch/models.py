from django.db import models


class Batch(models.Model):
    correlative = models.CharField(max_length=55)
    is_uploaded = models.BooleanField(default=False)
    is_paused = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now_add=True)
    date_updated = models.DateTimeField(auto_now=True)
    documents = models.ManyToManyField('documents.Document', blank=True)

    def get_number_of_documents_by_batch(self):
        obj = Batch.objects.get(pk=self.pk)
        return obj.documents.all().count()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None, *args, **kwargs):
        number = Batch.objects.all().count()+1
        self.correlative = 'G-' + str(number).zfill(8)
        super(Batch, self).save(force_insert, force_update, using, update_fields, *args, **kwargs)

    class Meta:
        ordering = ['-date_created']

from django.urls import path
from .views import BatchListView, BatchDetailView

urlpatterns = [
    path('',  BatchListView.as_view(), name='batch-list'),
    path('detail/<int:pk>/', BatchDetailView.as_view(), name='batch-detail')
]
from django.contrib import admin
from .models import Batch


@admin.register(Batch)
class BatchAdmin(admin.ModelAdmin):
    list_display = [
        'correlative', 'get_number_of_documents_by_batch', 'is_uploaded', 'is_paused']
    exclude = ['correlative']
